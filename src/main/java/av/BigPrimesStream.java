package av;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import scala.Int;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletionStage;

public class BigPrimesStream {
    public static void main(String[] args) {
        ActorSystem<?> actorSystem = ActorSystem.create(Behaviors.empty(), "actorSystem");
        List<BigInteger> list = new ArrayList<>(10);

        // emit 10 elements
        Source<Integer, NotUsed> tenElements = Source.range(1, 10);
        // map each element to Random BigInt, find next probable prime & add to list, order ascending
        Flow<Integer, List<BigInteger>, NotUsed> mapToRandomBigIntFlow = Flow.of(Integer.class)
                .grouped(10)
                .map(range -> {
                    for(Integer i : range) {
                        BigInteger bigInt = new BigInteger(2000, new Random());
                        list.add(bigInt.nextProbablePrime());
                    }
                    list.sort(Collections.reverseOrder());
                    return list;
                });
        // print list
        Sink<List<BigInteger>, CompletionStage<Done>> sink = Sink.foreach(System.out::println);

        tenElements.via(mapToRandomBigIntFlow).to(sink).run(actorSystem);

    }


}
